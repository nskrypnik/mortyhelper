import copy
import random
from lottery import checkIfMagicNumbers

_seq = range(1, 58)
# positive test
for i in range(10000): # tested on 1M
    seq = copy.copy(_seq)
    random.shuffle(seq)
    seq = seq[:7]
    sseq = ''.join([str(i) for i in seq])
    magic_numbers = checkIfMagicNumbers(sseq)
    assert magic_numbers != None, 'Next sequence is not recognized: %s' % seq

# negative test

_seq = range(60, 100) + [0 for i in xrange(20)] + [1 for i in xrange(20)]
for i in range(10000):
    seq = copy.copy(_seq)
    random.shuffle(seq)
    seq = seq[:7]
    sseq = ''.join([str(i) for i in seq])
    magic_numbers = checkIfMagicNumbers(sseq)
    if magic_numbers != None:
        print 'This sequence does make sense: ', seq, sseq, magic_numbers
