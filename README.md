This is a solution for a lottery problem:

```
Your favorite uncle, Morty, is crazy about the lottery and even crazier about how he picks his “lucky” numbers. And even though his “never fail” strategy has yet to succeed, Uncle Morty doesn't let that get him down.

Every week he searches through the Sunday newspaper to find a string of digits that might be potential lottery picks. But this week the newspaper has moved to a new electronic format, and instead of a comfortable pile of papers, Uncle Morty receives a text file with the stories.

Help your Uncle find his lotto picks. Given a large series of number strings, return each that might be suitable for a lottery ticket pick. Note that a valid lottery ticket must have 7 unique numbers between 1 and 59, digits must be used in order, and every digit must be used.

For example, given the following strings:

[ “1”, “42". “100848", “4938532894754”, “1234567”, “472844278465445”]

Your function should return:

4938532894754 -> 49 38 53 28 9 47 54
1234567 -> 1 2 3 4 5 6 7
```

## Problem solving notes

To solve this puzzle we have to analyse the problem first. So it's pretty obvious that the sequence we're looking for should be more then 7 symbols and less then 14 symbols. Then we may realize that for some exact amount of symbols in sequence we may expect exact amount of one digit and two digit numbers. E.g. for 14 symbol sequence we may expect 7 2-digit numbers, for 13 - 6 2-digit and 1 single-digit number, and so on. Basically we may easily calculate amount of number of every type for each occasion by next formula:

```
amount of single-digit numbers = 14 - amount of symbols
amount of 2-digit numbers = (amount of symbols - amount of single-digit numbers) / 2
```

So basically knowing the amount of single-digit numbers in sequence we may check all occasions of their position in sequence and if it satisfies certain conditions(like single-digit numbers should not duplicate or be zero etc.). To do this we'll use some cursors which will point on positions we assume single-digit numbers may be. Let's assume that `abcdefghijk` is a sequence(some of letters may represent same digits). It has 11 symbols so 3 single-digit numbers. Let's assume that they're all in begin of sequence:

```
  abcdefghijk
  ^^^
```

We may easily check whether this sequence is "magic numbers". But if not, we change cursor position then starting from the very right one. Each time we change cursor position we move it by 2 symbols as if we move it by 1 symbol we'll get discrepancy(if we move cursor to `d` it means that `c` should be a single-digit number as well which is impossible cause we would have 4 single-digit numbers in this case).

```
  abcdefghijk
  ^^  ^
```

We're moving cursor till it gets to last position (`k`), then if still we haven't succeed with our search we start to move next cursor to the left of right cursor. We move it in 2 position, but right cursor should follow next to it:

```
  abcdefghijk
  ^  ^^
```

So we repeat moving cursors in this way till we get all cursors on right side of the sequence:
```
  abcdefghijk
          ^^^
```
If we get to this position and still any checking reveals that sequence is a lottery numbers we don't suggest this sequence to uncle Morty then.

## Possible optimization

Further analyse may reveal some mathematical conditions which may guarantee that sequence is not the "magical numbers". E.g. if sequence starts from `0` it cannot be used as lottery numbers. This may be used for prevalidation before we run main algorithm but algorithm itself catch such conditions.

As another optimization which may be used - checking for a "bad 2-digit numbers". E.g. in next situation:
```
  abcdefghijk
    ^^^
```
If `ab` is greater then 59 it doesn't make sense to continue. Another case:
```
  abcdefghijk
      ^^^
```
If `ab` and `cd` are equal the sequence won't be useful for lottery ticket.

## Usage

To run a program use:

```
# python main.py [arg] ...
```

where arguments are just sequences we want to check. E.g.
```
# python main.py 1111456789 123456789 34345678911
```

## Files

* `main.py` - main program file
* `lottery.py` - a library which contains the implementation of algorithm
* `test.py` - simple unit-tests for `lottery` module
* `intgration_test.py` - tests with auto-generation of bad and good examples and testing over it
