import sys
from lottery import checkIfMagicNumbers

if __name__ == '__main__':
    if sys.argv > 1:
        for arg in sys.argv[1:]:
            magic_numbers = checkIfMagicNumbers(arg)
            if magic_numbers:
                print "%s -> %s" % (arg, ' '.join(magic_numbers))
