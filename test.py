from lottery import *

test1 = ['11', '1234567', '1234567890123456', '123456789012', '122345678900', '111156', '1234567890a']
test_result1 = [False, True, False, True, True, False, False]

for seq, res in zip(test1, test_result1):
    assert prevalidate(seq) == res, 'This is prevalidated incorrect: %s' % seq

print 'Prevalidation is correct'

# check getNumbersAmount

assert getSingleDigitNumbersAmount('1234567890') == 4, 'getSingleDigitNumbersAmount is incorrect'
print 'getSingleDigitNumbersAmount is correct'

assert SequenceIterator('123456789012').end_positions == [10, 11], 'end_positions are wrong'
assert SequenceIterator('123456789').end_positions == [4, 5, 6, 7, 8], 'end_positions are wrong'
assert SequenceIterator('12453432174251').end_positions == [], 'end_positions are wrong'
assert SequenceIterator('1234567').end_positions == [0, 1, 2, 3, 4, 5, 6], 'end_positions are wrong'
print 'end_positions are OK'

iterator = SequenceIterator('12345678901')
for i in range(4):
    iterator.next()
assert iterator.positions == [0, 1, 10], 'Iteration is wrong 1'
for i in range(4):
    iterator.next()
assert iterator.positions == [0, 3, 10], 'Iteration is wrong 1'
for i in range(4):
    iterator.next()
assert iterator.positions == [0, 7, 8], 'Iteration is wrong 1'
for i in range(4):
    iterator.next()
assert iterator.positions == [2, 3, 6], 'Iteration is wrong 1'
print 'Iteration is OK'

assert checkSingleDigitNumbers([1, 2, 6, 0]) == False, 'single-digit numbers check failed 1'
assert checkSingleDigitNumbers([1, 2, 6, 1]) == False, 'single-digit numbers check failed 2'
assert checkSingleDigitNumbers([1, 2, 6, 5]) == True, 'single-digit numbers check failed 3'
assert checkSingleDigitNumbers([1, 2, 2, 1, 1]) == False, 'single-digit numbers check failed 4'
assert checkSingleDigitNumbers([3, 2, 5, 1, 9]) == True, 'single-digit numbers check failed 5'
print 'Single-digit numbers check is OK'

assert getTwoDigitNumbers('12345678901', [2, 3, 6]) == [12, 56, 89, 1], 'Getting two-digit numbers failed 1'
assert getTwoDigitNumbers('12345678901', [0, 1, 10]) == [34, 56, 78, 90], 'Getting two-digit numbers failed 2'
assert getTwoDigitNumbers('32383671901', [4, 7, 8]) == [32, 38, 67, 1], 'Getting two-digit numbers failed 3'
print 'getTwoDigitNumbers is all right'

assert checkTwoDigitNumbers([4, 56, 12]) == False, 'checkTwoDigitNumbers failed 1'
assert checkTwoDigitNumbers([65, 56, 12, 17]) == False, 'checkTwoDigitNumbers failed 2'
assert checkTwoDigitNumbers([45, 56, 12, 17]) == True, 'checkTwoDigitNumbers failed 3'
assert checkTwoDigitNumbers([45, 56, 12, 17, 45]) == False, 'checkTwoDigitNumbers failed 4'
print 'checkTwoDigitNumbers is OK'

assert tryMagicNumbers('1234567', [0, 1, 2, 3, 4, 5, 6]) == True, 'tryMagicNumber failed 1'
assert tryMagicNumbers('12374567', [0, 1, 4, 5, 6, 7]) == True, 'tryMagicNumber failed 2'
assert tryMagicNumbers('12345678901', [0, 1, 10]) == False, 'tryMagicNumber failed 3'
assert tryMagicNumbers('12345678911', [6, 7, 8]) == True, 'tryMagicNumber failed 4'
assert tryMagicNumbers('12345678911', [6, 7, 8]) == True, 'tryMagicNumber failed 5'
assert tryMagicNumbers('01345678911', [6, 7, 8]) == False, 'tryMagicNumber failed 6'
assert tryMagicNumbers('56345678911', [6, 7, 8]) == False, 'tryMagicNumber failed 7'
assert tryMagicNumbers('12453432174251', []) == True, 'tryMagicNumber failed 8'
print 'tryMagicNumber is OK'

assert getMagicNumbers('1234567', [0, 1, 2, 3, 4, 5, 6]) == ['1', '2', '3', '4', '5', '6', '7'], 'getMagicNumbers failed 1'
assert getMagicNumbers('12345678911', [6, 7, 8]) == ['12', '34', '56', '7', '8', '9', '11'], 'getMagicNumbers failed 2'
assert getMagicNumbers('12453432174251', []) == ['12', '45', '34', '32', '17', '42', '51'], 'getMagicNumbers failed 3'
print 'getMagicNumbers is OK'

assert checkBadNumbers('12345678911', [6, 7, 8]) == False, 'checkBadNumbers failed 1'
assert checkBadNumbers('12345678911', [0, 7, 8]) == False, 'checkBadNumbers failed 2'
assert checkBadNumbers('12453432174251', []) == False, 'checkBadNumbers failed 3'
assert checkBadNumbers('89345678911', [6, 7, 8]) == True, 'checkBadNumbers failed 4'
assert checkBadNumbers('89345678911', [2, 7, 8]) == True, 'checkBadNumbers failed 5'
assert checkBadNumbers('09345678911', [2, 7, 8]) == True, 'checkBadNumbers failed 6'
assert checkBadNumbers('34345678911', [4, 7, 8]) == True, 'checkBadNumbers failed 7'
print 'checkBadNumbers is OK'

assert checkIfMagicNumbers('1234567') == ['1', '2', '3', '4', '5', '6', '7']
assert checkIfMagicNumbers('1230567') == None
assert checkIfMagicNumbers('12453432174251') == ['12', '45', '34', '32', '17', '42', '51']
assert checkIfMagicNumbers('12345678911') == ['12', '34', '56', '7', '8', '9', '11']
assert checkIfMagicNumbers('4938532894754') == ['49', '38', '53', '28', '9', '47', '54']
print 'checkIfMagicNumbers is OK'
