########################################################
# Constants
########################################################

MAX_LENGTH_OF_SEQ = 14
MIN_LENGTH_OF_SEQ = 7

def prevalidate(seq):
    '''
        This filters out all invalid sequences with length less than 7 and more
        than 14 as with
    '''
    # here you may apply some rules which may exclude appriory bad numbers just for
    # the sake of optimization as anyway algorythm will exclude it
    return not (len(seq) < MIN_LENGTH_OF_SEQ or len(seq) > MAX_LENGTH_OF_SEQ) and seq.isdigit()


def getSingleDigitNumbersAmount(seq):
    '''
        Returns amount of single-digit numbers which sequence may possibly contain
    '''
    return MAX_LENGTH_OF_SEQ - len(seq)


def checkSingleDigitNumbers(single_digit_numbers):
    '''
        Check list of single-digit numbers to be a valid sequence for lottery i.e.
        each number should be other than zero and no duplicate numbers
    '''
    for num in single_digit_numbers:
        if not num or single_digit_numbers.count(num) > 1:
            return False
    return True


def getTwoDigitNumbers(seq, cursor_positions):
    '''
        Get list of two-digit numbers from sequence according to assumption of
        single-digit numbers location i.e. cursor_positions value
    '''
    # first remove all assumed single-digit numbers
    _seq = ''.join([j for i, j in enumerate(seq) if i not in cursor_positions])
    return [int(_seq[i:i+2]) for i in range(0, len(_seq), 2)]


def checkTwoDigitNumbers(two_digit_numbers):
    '''
        Check a list of two digit numbers. If one of them is less then 10, or there're
        duplicate numbers or some number is more then 59 this sequence cannot be
        used for lottery
    '''
    for i in two_digit_numbers:
        if i < 10 or i > 59 or two_digit_numbers.count(i) > 1:
            return False
    return True


def tryMagicNumbers(seq, cursor_positions):
    '''
        With given positions of cursors (some positions we assume single-digit numbers
        may be) check whether entire sequence may be used as a lottery number
    '''
    single_digit_numbers = [int(seq[i]) for i in cursor_positions]
    if checkSingleDigitNumbers(single_digit_numbers):
        two_digit_numbers = getTwoDigitNumbers(seq, cursor_positions)
        if checkTwoDigitNumbers(two_digit_numbers):
            return True
    return False


def checkBadNumbers(seq, cursor_positions):
    '''
        In case when we moved very left cursor we check for "bad numbers" which might be left
        behind, like numbers greater then 59 or duplicated
    '''
    if not cursor_positions or cursor_positions[0] == 0:
        return False
    left_behind = seq[:cursor_positions[0]]
    left_behind = [int(left_behind[i:i+2]) for i in xrange(0, len(left_behind), 2)]
    return not checkTwoDigitNumbers(left_behind)


def getMagicNumbers(seq, cursor_positions):
    '''
        When Magic numbers are found we just get fetch them from sequence
    '''
    magic_numbers = []
    i = 0
    while len(seq) - i > 0:
        if i in cursor_positions:
            magic_numbers.append(seq[i])
            i += 1
        else:
            magic_numbers.append(seq[i: i+2])
            i += 2
    return magic_numbers


class SequenceIterator:
    '''
        This class exists because I'd like to incapsulate all itetration logic in one
        place, I might use recursion or but I think that code would be more elegant with
        class as with recusrion function we would have to pass too many parameters
    '''
    def __init__(self, seq):
        # We have to define how many 2-digit and 1-digit numbers given sequence
        # may contain
        one_digit_amount = getSingleDigitNumbersAmount(seq)
        self.positions = range(one_digit_amount)
        self.finished = False
        self.one_digit_amount = one_digit_amount
        self.end_positions = self.getEndPositions(seq)

    def getEndPositions(self, seq):
        '''
            Returns end positions for possible checking of single-digit numbers
        '''
        return [len(seq) - i for i in xrange(self.one_digit_amount, 0, -1)]

    def next(self):
        if self.finished or len(self.positions) == 0:
            self.finished = True
            return
        self._next(self.one_digit_amount - 1)

    def _next(self, pos_index):
        '''
            OK seems like we still need the recursion
        '''
        # check if position cursor reached it's last position
        if self.end_positions[pos_index] == self.positions[pos_index]:
            if pos_index == 0:
                # first checking pasition reached end
                self.finished = True
            else:
                # move previous position
                self._next(pos_index - 1)
                # move ahead of previous position
                self.positions[pos_index] = self.positions[pos_index - 1] + 1
        else:
            # move checking position for two numbers forward
            self.positions[pos_index] += 2


def checkIfMagicNumbers(seq):
    '''
        Our main function which gets a sequence of digits and then analyse whether
        it is a "magic" number i.e. some sequense which uncle Morty may play to
        improve his "non failure" statistics. Return magic number sequence or None
        if sequence is not a magic number
    '''
    if not prevalidate(seq):
        return False

    # get initial positions we want to check single-digit numbers on

    iterator = SequenceIterator(seq)

    while not iterator.finished:
        if tryMagicNumbers(seq, iterator.positions):
            return getMagicNumbers(seq, iterator.positions)
        if checkBadNumbers(seq, iterator.positions):
            # if we found bad numbers - doesn't make sense to continue
            return None
        iterator.next()
